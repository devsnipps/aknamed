class Post < ApplicationRecord
    mount_uploader :post_image, PostImageUploader
    belongs_to :user
    has_many :comments, as: :commentable

    validates :title, :description, :user_id, presence: true
end
