class CommentsController < ApplicationController
  before_action :authenticate_user!, :comment_params
  def create
  	post = Post.find(params["post_id"])
  	comment = post.comments.new(comment_params)
  	comment.user_id = current_user.id
  	comment.save!
  	redirect_to post_path(post)
  end

  private

  def comment_params
  	params.require(:comment).permit(:body, :post_id)
  end
end
