require "rails_helper"

RSpec.describe CommentsController, "#create new comment" do
	#before(:each) do
	let!(:post) { FactoryGirl.build(:post) }
	let!(:user) { FactoryGirl.build(:user) }
	#end

	it "Create comment" do
		post :create, comment: { body: "My comment", user_id: user.id, commentable_type: "Post", commentable_id: post.id}
	  	#post.comments << Comment.create(body: comment_body, user_id: user.id)
	  	expect(response).to redirect_to post_path(post)
	  	expect(conent).to eq "My comment"
	end
end