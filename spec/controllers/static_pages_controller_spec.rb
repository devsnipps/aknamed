require "rails_helper"

RSpec.describe StaticPagesController, "Test routes, templates" do
	describe "Static pages" do
	  it "Home page" do
	  	get :home
      	expect(response).to render_template("home")
	  end
	  it "About page" do
	  	get :about
      	expect(response).to render_template("about")
	  end
	end
end