require "rails_helper"
describe "Create post", type: :feature, js: true do
	let!(:post) { FactoryGirl.build(:post) }
	let!(:login_user) { FactoryGirl.build(:login_user) }

	before :each do
		visit "/users/sign_in"
		fill_in "user_email", with: login_user.email
		fill_in "user_password", with: login_user.password
		click_button "Log in"
	end

	it "Create post" do
		visit "/posts/new"
		sleep 1
		fill_in "post_title", with: post.title
		fill_in "post_description", with: post.description
		click_link "Create post"
		sleep 1
		expect(page).to have_content post.title
		sleep 2
	end

	it "Add a comment to first post" do
		visit "/posts/1"
		sleep 1
		fill_in "comment_description", with: "Feature test case comment"
		click_link "Create comment"
		sleep 4
		expect(page).to have_content "Feature test case comment"
		sleep 1
	end
end