require "rails_helper"
describe "Test static routes", type: :feature, js: true do
	it "Home page" do
		visit "/"
		expect(page).to have_content "Aknamed Blog"
		sleep 2
	end
	it "About page" do
		visit "/about"
		expect(page).to have_content "Akna Medical Private Limited"
		sleep 2
	end
end