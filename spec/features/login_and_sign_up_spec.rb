require "rails_helper"
describe "Login and signup", type: :feature, js: true do
	let!(:user) { FactoryGirl.build(:user) }
	let!(:login_user) { FactoryGirl.build(:login_user) }

	it "sign up" do
		visit "/users/sign_up"
		fill_in "user_email", with: user.email
		fill_in "user_password", with: user.password
		fill_in "user_password_confirmation", with: user.password
		click_button "Sign up"
		expect(page).to have_content "Aknamed Blog"
		sleep 3
		click_link "Logout"
		sleep 1
	end
	it "Login" do
		visit "/users/sign_in"
		fill_in "user_email", with: login_user.email
		fill_in "user_password", with: login_user.password
		click_button "Log in"
		sleep 3
		click_link "Logout"
		sleep 1
	end
end