FactoryGirl.define do
  factory :user, class: User do
  	sequence :email do |n|
    	"person#{n}@example.com"
    end
    password "password"
  end

  factory :login_user, class: User do
    email "person1@example.com"
    password "password"
  end
end 
