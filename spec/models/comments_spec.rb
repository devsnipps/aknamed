require "rails_helper"

RSpec.describe Comment, type: :model do
  let!(:user) { FactoryGirl.create(:user) }
  it { should belong_to(:user) }
  it { should belong_to(:commentable) }

  it { should validate_presence_of(:body) }

  context "with 2 or more comments" do
    it "orders them in reverse chronologically" do
      post = user.posts.create!(title: "post title", description: "post desc")
      comment1 = post.comments.create!(body: "first comment", user_id: user.id)
      comment2 = post.comments.create!(body: "second comment", user_id: user.id)
      expect(post.reload.comments).to eq([comment2, comment1])
    end
  end
end
